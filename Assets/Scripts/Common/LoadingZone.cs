using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingZone : MonoBehaviour
{
    public List<string> scenesToBeLoaded;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                string sceneName = SceneManager.GetSceneAt(i).name;
                if (sceneName != "Master" && !scenesToBeLoaded.Contains(sceneName))
                {
                    SceneManager.UnloadSceneAsync(sceneName);
                }
            }

            foreach (string name in scenesToBeLoaded)
            {
                if (!SceneManager.GetSceneByName(name).isLoaded)
                {
                    SceneManager.LoadSceneAsync("Scenes/OneButton/" + name, LoadSceneMode.Additive);
                }
            }
        }
    }
}
