using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraManager : MonoBehaviour
{
    private CinemachineVirtualCamera[] cameras;

    // Start is called before the first frame update
    void Start()
    {
        cameras = GetComponentsInChildren<CinemachineVirtualCamera>();
    }

    public void SwitchToCam(CinemachineVirtualCamera newCam)
    {
        foreach (CinemachineVirtualCamera camera in Resources.FindObjectsOfTypeAll<CinemachineVirtualCamera>())
        {
            camera.enabled = false;
        }

        newCam.enabled = true;
    }
}
