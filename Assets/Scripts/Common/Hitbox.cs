using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{
    public float knockbackForce = 1f;
    public float stunTime = 1f;
    public int damage = 1;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (gameObject.tag != other.tag)
        {
            other.gameObject.BroadcastMessage("OnHit", this, SendMessageOptions.DontRequireReceiver);
            gameObject.transform.root.gameObject.BroadcastMessage("OnDamageGiven", other.gameObject, SendMessageOptions.DontRequireReceiver);
        }
    }
}
