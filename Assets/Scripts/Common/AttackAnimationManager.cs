using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAnimationManager : MonoBehaviour
{
    public void OnAttackAnimationEnd()
    {
        transform.root.BroadcastMessage("OnAttackEnd", SendMessageOptions.DontRequireReceiver);
    }
}
