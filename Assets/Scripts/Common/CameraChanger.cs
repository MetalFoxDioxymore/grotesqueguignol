using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChanger : MonoBehaviour
{
    private CinemachineVirtualCamera cam;
    private CameraManager manager;

    private void Start()
    {
        cam = GetComponent<CinemachineVirtualCamera>();
        manager = GameObject.FindGameObjectWithTag("CameraManager").GetComponent<CameraManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            manager.SwitchToCam(cam);
        }
    }
}
