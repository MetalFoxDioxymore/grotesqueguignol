using Melanchall.DryWetMidi.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Fireworks : NoteSyncedSpawn
{
    public GameObject shrapnel;

    private Rigidbody2D body2D;

    void Start()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
        transform.position = new Vector2(Random.Range(-5f, 11f), -6);
        body2D = GetComponent<Rigidbody2D>();
        body2D.velocity = new Vector2(0, Random.Range(12f, 14f));
    }

    public void Explode(Note note)
    {
        if (note == spawnNote)
        {
            CreateShrapnel(Vector2.up);
            CreateShrapnel((Vector2.up + Vector2.left) * 0.7f);
            CreateShrapnel((Vector2.up + Vector2.right) * 0.7f);
            CreateShrapnel(Vector2.left);
            CreateShrapnel(Vector2.right);
            Destroy(gameObject);
        }
    }

    private void CreateShrapnel(Vector2 direction)
    {
        GameObject newShrapnel = Instantiate(shrapnel);
        newShrapnel.transform.position = transform.position;
        shrapnel.GetComponent<FireworkShrapnel>().direction = direction;
    }
}
