using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightBlink : AutowiredMonoBehaviour
{
    private Light2D light2d;

    public void Pulse(int beatNumber)
    {
        light2d.intensity = 1;
        StartCoroutine(DecreaseIntensity());
        
    }

    IEnumerator DecreaseIntensity()
    {
        while (light2d.intensity > 0f)
        {
            yield return new WaitForSeconds(0.05f);
            light2d.intensity -= 0.15f;
        }
    }
}
