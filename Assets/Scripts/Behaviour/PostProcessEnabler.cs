using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessEnabler : MonoBehaviour
{
    public PostProcessVolume postProcessVolume;
    public int startBeat = 0;

    // Start is called before the first frame update
    void Start()
    {
        postProcessVolume.weight = 0;
    }

    public void Enable(int beatNumber)
    {
        if (beatNumber > startBeat)
        {
            postProcessVolume.weight = 1;
        }
    }
}
