using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Melanchall.DryWetMidi.Interaction;

public class Spawner : MonoBehaviour
{
    public GameObject target;

    public void Spawn(Note note)
    {
        GameObject go = Instantiate(target);
        NoteSyncedSpawn nss = go.GetComponent<NoteSyncedSpawn>();
        if (nss != null)
        {
            nss.SetSpawnNote(note);
        }
    }
}
