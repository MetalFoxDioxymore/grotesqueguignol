using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BellWave : NoteSyncedSpawn
{
    private Rigidbody2D body2D;

    void Start()
    {
        body2D = GetComponent<Rigidbody2D>();
        body2D.velocity = new Vector2(0, 8f);

        float normal = Mathf.InverseLerp(55, 65, spawnNote.NoteNumber);
        float xPosition = Mathf.Lerp(-5, 11, normal);

        transform.position = new Vector2(xPosition, -6f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
