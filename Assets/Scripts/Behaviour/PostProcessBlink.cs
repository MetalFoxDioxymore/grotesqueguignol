using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessBlink : MonoBehaviour
{
    public PostProcessVolume postProcessVolume;

    // Start is called before the first frame update
    void Start()
    {
        postProcessVolume.weight = 0;
    }

    public void Blink(int beatNumber)
    {
        if (beatNumber == 41 || beatNumber == 81)
        {
            postProcessVolume.weight = 1;
            StartCoroutine(DecreaseBlink());
        }
    }

    IEnumerator DecreaseBlink()
    {
        while (postProcessVolume.weight > 0f)
        {
            yield return new WaitForSeconds(0.1f);
            postProcessVolume.weight -= 0.1f;
        }
    }
}
