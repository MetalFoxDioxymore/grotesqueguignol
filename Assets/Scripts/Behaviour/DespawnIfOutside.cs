using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnIfOutside : MonoBehaviour
{
    void Update()
    {
        if (transform.position.y < -20f || transform.position.y > 20f)
        {
            Destroy(gameObject);
        }
    }
}
