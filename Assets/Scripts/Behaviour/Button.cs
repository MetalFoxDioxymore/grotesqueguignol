using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Button : MonoBehaviour
{
    public UnityEvent onPressed;

    private bool pressed = false;

    private void Start()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<OneButtonPlayerController>().onPlayerDeath.AddListener(Reset);
    }

    private void Reset()
    {
        if (pressed)
        {
            pressed = false;
            onPressed.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!pressed && collision.tag == "Player")
        {
            onPressed.Invoke();
            pressed = true;
        }
    }
}
