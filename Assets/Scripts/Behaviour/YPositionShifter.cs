using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YPositionShifter : MonoBehaviour
{
    public float distance;
    public float speed;

    private float yStart;
    private bool shifted = false;

    private void Start()
    {
        yStart = transform.position.y;
    }

    private void FixedUpdate()
    {
        if (shifted && (transform.position.y - yStart + distance) > 0f)
        {
            transform.Translate(0, -Mathf.Min(speed * Time.fixedDeltaTime, transform.position.y - yStart + distance), 0);
        }

        if (!shifted && (transform.position.y - yStart) < 0f)
        {
            transform.Translate(0, Mathf.Min(speed * Time.fixedDeltaTime, Mathf.Abs(transform.position.y - yStart)), 0);
        }
    }

    public void Shift()
    {
        shifted = !shifted;
    }
}
