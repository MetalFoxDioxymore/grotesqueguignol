using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class JuggleBall : MonoBehaviour
{
    private Rigidbody2D body2D;

    void Start()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
        transform.position = new Vector2(Random.Range(mousePosition.x - 3f, mousePosition.x + 3f), -6);
        body2D = GetComponent<Rigidbody2D>();

        if (transform.position.x < mousePosition.x)
        {
            body2D.velocity = new Vector2(Random.Range(1f, 3f), 10);
        }
        else
        {
            body2D.velocity = new Vector2(Random.Range(-1f, -3f), 10);
        }
    }
}
