using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireworkShrapnel : MonoBehaviour
{
    public Vector2 direction;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = direction * 5f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
