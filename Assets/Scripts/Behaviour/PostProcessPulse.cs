using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessPulse : MonoBehaviour
{
    public PostProcessVolume postProcessVolume;
    public int startBeat = 0;

    // Start is called before the first frame update
    void Start()
    {
        postProcessVolume.weight = 0;
    }

    public void Pulse(int beatNumber)
    {
        if (beatNumber > startBeat)
        {
            postProcessVolume.weight = 1;
            StartCoroutine(DecreasePulse());
        }
    }

    IEnumerator DecreasePulse()
    {
        while (postProcessVolume.weight > 0f)
        {
            yield return new WaitForSeconds(0.05f);
            postProcessVolume.weight -= 0.1f;
        }
    }
}
