using Cinemachine;
using Melanchall.DryWetMidi.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Laser : NoteSyncedSpawn
{
    private LineRenderer lineRenderer;
    private CinemachineImpulseSource impulse;

    void Start()
    {
        impulse = GetComponent<CinemachineImpulseSource>();
        lineRenderer = GetComponent<LineRenderer>();
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
        lineRenderer.SetPositions(new Vector3[] { 
            new Vector3(Mathf.Clamp(Random.Range(mousePosition.x - 2f, mousePosition.x + 2f), -5f, 11f), -20f, 0), 
            new Vector3(Mathf.Clamp(Random.Range(mousePosition.x - 3f, mousePosition.x + 3f), -5f, 11f), 20f, 0) });
        lineRenderer.startWidth = 0.05f;
        lineRenderer.endWidth = 0.05f;
    }

    public void Fire(Note note)
    {
        if (note == spawnNote)
        {
            impulse.GenerateImpulse();
            Vector3[] positions = new Vector3[2];
            lineRenderer.GetPositions(positions);
            RaycastHit2D[] hits = Physics2D.LinecastAll(positions[0], positions[1]);
            foreach (RaycastHit2D hit in hits)
            {
                if (hit && hit.collider.gameObject.GetComponent<Hit>() != null)
                {
                    hit.collider.gameObject.GetComponent<Hit>().Hurt();
                }
            }

            lineRenderer.startWidth = 0.5f;
            lineRenderer.endWidth = 0.5f;
            StartCoroutine(TerminateFiring());
        }
    }

    public IEnumerator TerminateFiring()
    {
        while (lineRenderer.startWidth > 0.04f)
        {
            yield return new WaitForSeconds(0.01f);
            lineRenderer.startWidth -= 0.01f;
            lineRenderer.endWidth -= 0.01f;

        }
        Destroy(gameObject);
    }
}
