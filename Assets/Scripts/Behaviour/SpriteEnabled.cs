using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteEnabled : MonoBehaviour
{
    private SpriteRenderer sprite;

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    public void Appear(int beatNumber)
    {
        if (beatNumber == 81)
        {
            sprite.enabled = true;
        }
    }
}
