using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerAnimation
{
    public const string PLAYER_IDLE = "Idle";
    public const string PLAYER_RUN = "Run";
    public const string PLAYER_FALL = "Fall";
    public const string PLAYER_JUMP = "Jump";
    public const string PLAYER_JAB = "Attack1";
}

public class PlayerAnimator : AnimationController
{
    public SpriteRenderer spriteRenderer;

    private Rigidbody2D body;
    private CollisionManager collision;

    void Update()
    {
        if (currentState != PlayerAnimation.PLAYER_JAB)
        {
            if (collision.IsTouchingDown())
            {
                ChangeAnimationState(GetGroundedAnimation());
            }
            else
            {
                ChangeAnimationState(GetAirborneAnimation());
            }
        }
    }

    public void Jab()
    {
        ChangeAnimationState(PlayerAnimation.PLAYER_JAB);
    }

    private string GetGroundedAnimation()
    {
        if (Mathf.Abs(body.velocity.x) > 0.01f)
        {
            return PlayerAnimation.PLAYER_RUN; 
        }

        return PlayerAnimation.PLAYER_IDLE;
    }

    private string GetAirborneAnimation()
    {
        if (body.velocity.y >= 0f)
        {
            return PlayerAnimation.PLAYER_JUMP;
        }

        return PlayerAnimation.PLAYER_FALL;
    }

    private void OnAttackEnd()
    {
        ChangeAnimationState(GetGroundedAnimation());
    }
}
