using System.Collections;
using UnityEngine;

public class EnemyController : AutowiredMonoBehaviour
{
    public float distance;

    private float initialXPosition;
    private HorizontalMovement movement;
    private float direction = 1f;
    private Coroutine killCoroutine;
    private ParticleSystem deathParticles;

    private void Start()
    {
        initialXPosition = transform.position.x;
        GameObject.FindGameObjectWithTag("Player").GetComponent<OneButtonPlayerController>().onPlayerDeath.AddListener(Respawn);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(transform.position.x - distance, transform.position.y), new Vector3(transform.position.x - distance, transform.position.y));
    }

    void FixedUpdate()
    {
        if (transform.position.x <= initialXPosition - distance) {
            direction = 1;
        }
        else if (transform.position.x >= initialXPosition + distance)
        {
            direction = -1;
        }

        movement.Move(direction);
    }

    public void Kill()
    {
        killCoroutine = StartCoroutine(KillCoroutine());
    }

    IEnumerator KillCoroutine()
    {
        deathParticles.Play();
        Disable();
        yield return new WaitForSeconds(10f);
        Respawn();
    }

    private void Disable()
    {
        for (int i = 0; i < transform.childCount; i++) {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    private void Respawn()
    {
        for (int i = 0; i < transform.childCount; i++){
            transform.GetChild(i).gameObject.SetActive(true);
        }

        if (killCoroutine != null)
        {
            StopCoroutine(killCoroutine);
        }
    }
}
