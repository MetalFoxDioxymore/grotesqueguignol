using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

enum PlayerState
{
    ACTIONABLE, ATTACKING, STUNNED
}

public class PlayerController : AutowiredMonoBehaviour
{
    public float jumpForce;

    private HorizontalMovement movement;
    private Facing facing;
    private MusicManager musicManager;
    private CollisionManager collision;
    private PlayerAnimator playerAnimator; 
    private Jump jump;
    private PlayerState state = PlayerState.ACTIONABLE;

    void Start()
    {
        musicManager = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<MusicManager>();
    }

    private void OnMove(InputValue value)
    {
        float direction = value.Get<float>();
        movement.Move(direction);
        facing.ChangeDirection(direction);
    }

    private void OnJump(InputValue value)
    {
        if (value.Get<float>() > 0 && collision.IsTouchingDown() && state == PlayerState.ACTIONABLE)
        {
            if (musicManager.isThisFrameInBeat())
            {
                jump.StartJump(jumpForce * 1.5f);
            }
            else
            {
                jump.StartJump(jumpForce);
            }
        }
    }

    private void OnAttack(InputValue value)
    {
        movement.Root();
        facing.Lock("attack");
        state = PlayerState.ATTACKING;
        playerAnimator.Jab();
    }

    private void OnAttackEnd()
    {
        movement.Unroot();
        facing.Unlock("attack");
        state = PlayerState.ACTIONABLE;
    }

    private void OnLeavingDown()
    {
        facing.Lock("airborne");
    }

    private void OnTouchingDown()
    {
        facing.Unlock("airborne");
    }
}
