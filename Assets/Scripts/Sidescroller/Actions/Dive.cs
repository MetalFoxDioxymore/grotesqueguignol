using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dive : AutowiredMonoBehaviour
{
    public float diveSpeed;

    private Rigidbody2D body;

    public void StartDive()
    {
        body.velocity = new Vector2(body.velocity.x, -diveSpeed);
    }
}
