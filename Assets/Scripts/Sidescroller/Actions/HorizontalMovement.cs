using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalMovement : AutowiredMonoBehaviour
{
    public float walkingSpeed;
    public float acceleration;

    private float currentSpeed = 0;
    private Rigidbody2D body;
    private float direction = 0;
    private bool isRooted = false;
    private bool isAccelerating = true;

    private void FixedUpdate()
    {
        if (isAccelerating)
        {
            currentSpeed = isRooted ? 0 : Mathf.Min(currentSpeed + acceleration * Time.fixedDeltaTime, walkingSpeed);
        }

        body.velocity = (new Vector2(currentSpeed * direction, body.velocity.y));
    }

    public void SetMaxSpeed()
    {
        currentSpeed = walkingSpeed;
    }

    public void SetMinSpeed(float minimumSpeed)
    {
        currentSpeed = Mathf.Max(currentSpeed, minimumSpeed);
    }

    public void Accelerate()
    {
        isAccelerating = true;
    }

    public void KeepConstantSpeed()
    {
        isAccelerating = false;
    }

    public void ResetSpeed()
    {
        currentSpeed = 0;
    }

    public void Move(float newDirection)
    {
        direction = newDirection;
    }

    public void Root()
    {
        isRooted = true;
    }

    public void Unroot()
    {
        isRooted = false;
    }
}
