using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : AutowiredMonoBehaviour
{
    public float maxReleaseForce;

    private Rigidbody2D body;
    private bool cancelable = true;

    public void StartJump(float jumpForce)
    {
        cancelable = true;
        DoJump(jumpForce);
    }

    public void StartFixedJump(float jumpForce)
    {
        cancelable = false;
        DoJump(jumpForce);
    }

    private void DoJump(float jumpForce)
    {
        body.velocity = new Vector2(body.velocity.x, jumpForce);
    }

    public void StopJump()
    {
        if (cancelable && body.velocity.y > maxReleaseForce)
        {
            body.velocity = new Vector2(body.velocity.x, maxReleaseForce);
        }
    }
}
