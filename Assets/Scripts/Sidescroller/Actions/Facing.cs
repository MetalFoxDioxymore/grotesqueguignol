using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Facing : MonoBehaviour
{
    private float bufferedDirection = 1;
    private List<string> lockVotes = new List<string>();

    private void FixedUpdate()
    {
        if (bufferedDirection != transform.localScale.x)
        {
            ChangeScale();
        }
    }

    public void ChangeDirection(float newDirection)
    {
        if (newDirection == -1f || newDirection == 1f)
        {
            bufferedDirection = newDirection;
        }
        else
        {
            bufferedDirection = transform.localScale.x;
        }
    }

    private void ChangeScale()
    {
        if (lockVotes.Count == 0)
        {
            transform.localScale = new Vector3(bufferedDirection, 1, 1);
        }
    }

    public void Lock(string voterName)
    {
        if (!lockVotes.Contains(voterName))
        {
            lockVotes.Add(voterName);
        }
    }

    public void Unlock(string voterName)
    {
        lockVotes.Remove(voterName);
    }
}
