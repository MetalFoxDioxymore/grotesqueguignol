using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class GameStarter : MonoBehaviour
{
    public bool gameStarted { get; private set; } = false;
    public UnityEvent onGameStart { get; private set; } = new UnityEvent();
    public GameObject messages;
    public GameObject hitCounter;
    public SpriteRenderer circus;

    private bool allowClicking = false;

    private void Start()
    {
        StartCoroutine(AllowClicking());
    }

    void OnClick(InputValue value)
    {
        if (allowClicking && !gameStarted)
        {
            gameStarted = true;
            messages.SetActive(false);
            StartCoroutine(FadeIn());
        }
    }

    IEnumerator AllowClicking()
    {
        yield return new WaitForSeconds(1f);
        allowClicking = true;
    }

    IEnumerator FadeIn()
    {
        while (circus.color.a < 1f)
        {
            yield return new WaitForSeconds(0.1f);
            circus.color = new Color(1f, 1f, 1f, circus.color.a + 0.05f);
        }

        hitCounter.SetActive(true);
        onGameStart.Invoke();
    }
}
