using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FollowMouse : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMove(InputValue value)
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(value.Get<Vector2>());
        mousePosition.x = Mathf.Clamp(mousePosition.x, -5, 11);
        mousePosition.y = Mathf.Clamp(mousePosition.y, -5, 4);
        mousePosition.z = 0;
        transform.position = mousePosition;
    }
}