using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Hit : MonoBehaviour
{
    public int hitNumber { get; private set; } = 0;
    public UnityEvent<int> onHit { get; private set; } = new UnityEvent<int>();

    public void Hurt()
    {
        hitNumber++;
        onHit.Invoke(hitNumber);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Hurt();
    }
}
