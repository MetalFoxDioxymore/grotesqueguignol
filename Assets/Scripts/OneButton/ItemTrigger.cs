using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public enum Item
{
    WALLJUMP, DIVE
}

public class ItemTrigger : MonoBehaviour
{
    public Item item;
    public string itemGotSound;
    public string musicPhase;
    public GameObject text;
    public CinemachineVirtualCamera cutsceneCamera;
    public PostProcessVolume blink;

    private MusicSwitcher musics;
    private MusicSwitcher sfx;

    private void Start()
    {
        musics = GameObject.FindGameObjectWithTag("Musics").GetComponent<MusicSwitcher>();
        sfx = GameObject.FindGameObjectWithTag("Sfxs").GetComponent<MusicSwitcher>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            StartCoroutine(StartPickup(collision.gameObject));
        }
    }

    private IEnumerator StartPickup(GameObject player)
    {
        cutsceneCamera.enabled = true;
        Rigidbody2D body = player.GetComponent<Rigidbody2D>();
        body.simulated = false;
        while (Vector3.Distance(player.transform.position, transform.position) > 0.01f)
        {
            player.transform.position = Vector3.MoveTowards(player.transform.position, transform.position, Time.fixedDeltaTime);
            yield return new WaitForFixedUpdate();
        }

        Pickup(player);
        GetComponent<BoxCollider2D>().enabled = false;
        sfx.Play(itemGotSound);
        text.SetActive(true);
        musics.ActivateMusic(musicPhase);
        blink.weight = 1f;
        while (blink.weight > 0f)
        {
            blink.weight -= 2f * Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        yield return new WaitForSeconds(2f);
        body.simulated = true;
        cutsceneCamera.enabled = false;
    }

    private void Pickup(GameObject player)
    {

        ItemManager items = player.GetComponent<ItemManager>();
        switch (item)
        {
            case Item.DIVE:
                items.AcquireDive();
                break;
            case Item.WALLJUMP:
                items.AcquireWalljump();
                break;
        }
    }

}
