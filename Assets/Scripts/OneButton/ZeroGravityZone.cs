using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class ZeroGravityZone : MonoBehaviour
{
    public AudioMixerGroup zeroG;
    public AudioMixerGroup normal;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
            foreach (AudioSource source in FindObjectsOfType<AudioSource>())
            {
                source.outputAudioMixerGroup = zeroG;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1.8f;
            foreach (AudioSource source in FindObjectsOfType<AudioSource>())
            {
                source.outputAudioMixerGroup = normal;
            }
        }
    }
}
