using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSwitcher : MonoBehaviour
{
    private AudioSource[] musics;

    private void Start()
    {
        musics = GetComponentsInChildren<AudioSource>();
    }

    public void SwitchMusic(string name)
    {
        foreach (AudioSource currentMusic in musics)
        {
            if (currentMusic.name == name)
            {
                currentMusic.Stop();
                currentMusic.Play();
                currentMusic.volume = 1f;
            }
            else
            {
                currentMusic.volume = 0f;
            }
        }
    }

    public void ActivateMusic(string name)
    {
        foreach (AudioSource currentMusic in musics)
        {
            if (currentMusic.name == name)
            {
                currentMusic.volume = 1f;
            }
        }
    }

    public void Play(string name)
    {
        foreach (AudioSource currentMusic in musics)
        {
            if (currentMusic.name == name)
            {
                currentMusic.volume = 1f;
            }
        }
    }
}
