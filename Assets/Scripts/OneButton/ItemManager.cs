using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public GameObject diveSkin;
    public GameObject wallJumpSkin;

    public bool hasWalljump { get; private set; } = false;
    public bool hasDive { get; private set; } = false;

    public void AcquireWalljump()
    {
        hasWalljump = true;
        wallJumpSkin.SetActive(true);
    }

    public void AcquireDive()
    {
        hasDive = true;
        diveSkin.SetActive(true);
    }

    public bool HasAllItems()
    {
        return hasWalljump && hasDive;
    }

    void OnCheat()
    {
        AcquireWalljump();
        AcquireDive();
    }
}
