using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using static UnityEngine.ParticleSystem;

public class OneButtonPlayerController : AutowiredMonoBehaviour
{
    public float minimumAirborneSpeed = 0.5f;
    public float jumpForce;
    public float walljumpForce;
    public float reboundForce;
    public ParticleSystem deathParticles;
    public AudioSource jumpSound;
    public AudioSource enemyDeathSound;
    public AudioSource diveSound;
    public AudioSource playerDeathSound;
    public UnityEvent onPlayerDeath { get; private set; } = new UnityEvent();
    public GameObject lastCheckpoint;

    private float direction = 1;
    private bool diving = false;

    private HorizontalMovement movement;
    private Jump jump;
    private CollisionManager collisions;
    private Dive dive;
    private ItemManager items;
    private GameObject currentCheckpoint;
    private float currentCheckpointDirection;
    private Particle[] particles;
    private Rigidbody2D body2D;
    private ParticleSystem dust;

    // Start is called before the first frame update
    void Start()
    {
        particles = new Particle[deathParticles.main.maxParticles];
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!diving && body2D.velocity.y < -dive.diveSpeed)
        {
            body2D.velocity = new Vector2(body2D.velocity.x, -dive.diveSpeed);
        }

        if (!collisions.IsTouchingDown())
        {
            movement.SetMinSpeed(minimumAirborneSpeed);
        }

        movement.Move(direction);
        if (CollidesWithWall())
        {
            movement.ResetSpeed();
            if (collisions.IsTouchingDown())
            {
                direction *= -1;
            }
        }

        if (Mathf.Abs(body2D.velocity.x) < movement.walkingSpeed - 0.001f)
        {
            MainModule dustModule = dust.main;
            dustModule.startColor = Color.gray;
        } 
        else
        {
            MainModule dustModule = dust.main;
            dustModule.startColor = Color.yellow;
        }
    }

    private void OnAction(InputValue value)
    {
        bool isPressed = value.Get<float>() > 0;

        if (isPressed)
        {
            if (collisions.IsTouchingDown())
            {
                jumpSound.Play();
                jump.StartJump(jumpForce);
            }
            else // Not touching ground
            {
                if (items.hasWalljump && CollidesWithWall()) //Walljump
                {
                    jumpSound.Play();
                    direction *= -1;
                    jump.StartJump(walljumpForce);
                    diving = false;
                    movement.SetMaxSpeed();
                }
                else if (items.hasDive && !diving)
                {
                    diveSound.Play();
                    dive.StartDive();
                    diving = true;
                }
            }
        }
        else
        {
            if (body2D.gravityScale > 0)
            {
                jump.StopJump();
            }
        }
        
    }

    private bool CollidesWithWall()
    {
        return direction == -1 && collisions.IsTouchingLeft() || direction == 1 && collisions.IsTouchingRight();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            if (diving) {
                enemyDeathSound.Play();
                jump.StartFixedJump(reboundForce);
                diving = false;
                collision.gameObject.GetComponentInParent<EnemyController>().Kill();
            } 
            else
            {
                Die();
            }
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Instadeath"))
        {
            Die();
        }

        if (collision.gameObject.tag == "Checkpoint" && collision.gameObject != currentCheckpoint)
        {
            if (collision.gameObject != lastCheckpoint || items.HasAllItems())
            {
                currentCheckpoint = collision.gameObject;
                currentCheckpointDirection = direction;
                onPlayerDeath.Invoke();
            }
        }
    }

    void Die()
    {
        dust.Stop();
        onPlayerDeath.Invoke();
        StartCoroutine(DieCoroutine());
    }

    IEnumerator DieCoroutine()
    {
        Array.Clear(particles, 0, particles.Length);
        playerDeathSound.Play();
        Disable();
        float speed = 1f;
        deathParticles.Play();
        while (deathParticles.isEmitting)
        {
            InitializeDeathParticles();
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        while (!MoveDeathParticles(Time.fixedDeltaTime * speed))
        {
            speed += 1f;
            yield return new WaitForFixedUpdate();
        }
        deathParticles.Clear();
        Respawn();
    }

    void InitializeDeathParticles()
    {
        int numParticlesAlive = deathParticles.GetParticles(particles);
        for (int i = 0; i < numParticlesAlive; i++)
        {
            if (Vector3.Distance(particles[i].position, transform.position) > 5f)
            {
                particles[i].position = transform.position;
            }
        }
        deathParticles.SetParticles(particles);
    }

    bool MoveDeathParticles(float speed)
    {
        bool allParticlesArrived = true;
        int numParticlesAlive = deathParticles.GetParticles(particles);
        for (int i = 0; i < numParticlesAlive; i++)
        {
            particles[i].position = Vector3.MoveTowards(particles[i].position, currentCheckpoint.transform.position, speed);
            if (Vector3.Distance(particles[i].position, currentCheckpoint.transform.position) > 0.1f)
            {
                allParticlesArrived = false;
            }
        }
        deathParticles.SetParticles(particles);

        return allParticlesArrived;
    }

    private void Disable()
    {
        body2D.simulated = false;
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    void Respawn()
    {
        movement.ResetSpeed();
        direction = currentCheckpointDirection;
        body2D.velocity = Vector2.zero;
        transform.position = currentCheckpoint.transform.position;
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
        body2D.simulated = true;
    }

    void OnTouchingDown()
    {
        movement.Accelerate();
        diving = false;
        dust.Play();
    }

    void OnLeavingDown()
    {
        movement.KeepConstantSpeed();
        dust.Stop();
    }

}
