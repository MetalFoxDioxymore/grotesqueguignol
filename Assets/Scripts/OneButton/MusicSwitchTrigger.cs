using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSwitchTrigger : MonoBehaviour
{
    public string music;

    private MusicSwitcher switcher;

    void Start()
    {
        switcher = GameObject.FindGameObjectWithTag("Musics").GetComponent<MusicSwitcher>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            switcher.SwitchMusic(music);
        }
    }
}
