using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class AutowiredMonoBehaviour : MonoBehaviour
{
    void Awake()
    {
        foreach (FieldInfo info in GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic))
        {
            if (info.FieldType.IsSubclassOf(typeof(MonoBehaviour)) || info.FieldType.IsSubclassOf(typeof(Component)))
            {
                info.SetValue(this, GetComponent(info.FieldType));
            }
        }
    }
}
