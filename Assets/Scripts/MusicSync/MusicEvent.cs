using Melanchall.DryWetMidi.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MusicEventType
{
    OnNotePlayed,
    OnOneBeatBeforePlayed,
    OnBeat,
}

public class MusicEvent
{
    public Note note;
    public MusicEventType type;

    public MusicEvent(Note note, MusicEventType type)
    {
        this.note = note;
        this.type = type;
    }
}
