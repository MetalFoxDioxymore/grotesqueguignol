using Melanchall.DryWetMidi.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteSyncedSpawn : MonoBehaviour
{
    protected Note spawnNote;

    public void SetSpawnNote(Note note)
    {
        spawnNote = note;
    }
}
