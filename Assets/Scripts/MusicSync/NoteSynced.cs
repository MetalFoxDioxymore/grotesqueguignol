using Melanchall.DryWetMidi.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class NoteSynced : MonoBehaviour
{
    public string channelNumber = "0";
    public UnityEvent<Note> onNoteCallback;
    public UnityEvent<Note> onOneBeatBeforeNoteCallback;
    public UnityEvent<int> onBeat;
    public UnityEvent onMusicEnd;

    private MusicManager musicManager;

    private void Start()
    {
        musicManager = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<MusicManager>();
        musicManager.onNotePlayed.AddListener(SendOnNoteMessage);
        musicManager.OnOneBeatBeforePlayed.AddListener(SendOnOneBeatBeforeNoteMessage);
        musicManager.OnBeat.AddListener(SendOnBeatMessage);
        musicManager.OnMusicEnd.AddListener(SendOnMusicEndMessage);
    }

    public void SendOnNoteMessage(Note note)
    {
        if (note.Channel.ToString() == channelNumber)
        {
            onNoteCallback.Invoke(note);
        }
    }

    public void SendOnOneBeatBeforeNoteMessage(Note note)
    {
        if (note.Channel.ToString() == channelNumber)
        {
            onOneBeatBeforeNoteCallback.Invoke(note);
        }
    }

    public void SendOnBeatMessage(int beatNumber)
    {
        onBeat.Invoke(beatNumber);
    }

    public void SendOnMusicEndMessage()
    {
        onMusicEnd.Invoke();
    }
}
