using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Melanchall.DryWetMidi.Common;
using Melanchall.DryWetMidi.Core;
using Melanchall.DryWetMidi.Interaction;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.IO;

public class MusicManager : MonoBehaviour
{
    public UnityEvent<Note> onNotePlayed { get; private set; } = new UnityEvent<Note>();
    public UnityEvent<Note> OnOneBeatBeforePlayed { get; private set; } = new UnityEvent<Note>();
    public UnityEvent<int> OnBeat { get; private set; } = new UnityEvent<int>();
    public UnityEvent OnMusicEnd { get; private set; } = new UnityEvent();

    public AudioSource audioSource;
    public AudioSource drumStick;

    private SortedList<float, List<MusicEvent>> allMusicEvents = new SortedList<float, List<MusicEvent>>();
    private int currentBeat;
    private float tempoInterval;
    private bool loaded = false;
    private float timeStart;

    // Start is called before the first frame update
    void Start()
    {
        Load("GrotesqueGuignol");
    }

    public void Load(string music)
    {
        allMusicEvents.Clear();
        StartCoroutine(LoadMidiFile(music));
        Play();
    }

    private IEnumerator LoadMidiFile(string music)
    {
        /** 
         * Okay, so, this is interesting and I lost way too much time on it to not explain.
         * 
         * DryWetMidi needs the MIDI file path to be able to parse it. However, by default, Unity packs all the files into some "asset resources bundle", 
         * and the final folder architecture of the built game is completely different than the one in the editor, so the file itself cannot be used by the library.
         * 
         * Here comes the Streaming Assets to the rescue (https://docs.unity3d.com/Manual/StreamingAssets.html) ! "Streaming Assets" are files that won't 
         * be packed into that final bundle, but will be left as-is. To use the Streaming Assets, it's simple : create a "StreamingAssets" folder in the "Assets"
         * folder of the project and tadaaaaa ! You can now access pretty much everything there is inside like normal, without having to fight with Unity bundling
         * tendancies. The final path of that "streaming assets folder" depends of the OS, but you can use Application.streamingAssetsPath to retrieve it.
         * 
         * So, we just put the MIDI file in the StreamingAssets and retrieve it with Application.streamingAssetsPath + "myMidiFile.mid" and that's it ? Well,
         * yeah, but actually no. That works perfectly for desktop applications, but when you try that on a WebGL build, an error shows up. This is because, 
         * for security reasons, you WebGL application doesn't have a direct access to the files that are located on the server (which is a good thing, now
         * that I think about it xD. Otherwise, you could just delete everything on itch.io). It means that, before being able to use a file located 
         * in StreamingAssets, you first need to download it in a folder the game has access too (like Application.persistentDataPath, for persistent data,
         * or Application.temporaryCachePath for temp data), and use that downloaded file (explained here : 
         * https://docs.unity3d.com/ScriptReference/Application-streamingAssetsPath.html, 
         * "It is not possible to access the StreamingAssets folder on WebGL and Android platforms.")
         * 
         * To do so, Unity has a class called UnityWebRequest (https://docs.unity3d.com/Manual/UnityWebRequest.html), that is basically an asyncronous HTTP client
         * (you can do GET, POST, PUT, etc. requests).
         * Depending of what kind of response the server send back, you can attach different DownloadHandlers to your UnityWebRequest that helps you manage it
         * (https://docs.unity3d.com/Manual/UnityWebRequest-CreatingDownloadHandlers.html). If you just request an API that returns JSON data, you won't need it,
         * but in our case, the server returns a whole file that we need to save in a folder, and Unity provides a DownloadHandler that does exactly that :
         * the DownloadHandlerFile.
         * 
         * The cool thing is that, even if it's a fix used for the WebGL version, it also works for the dekstop ones, so you can use the same code everywhere.
         * Using the Streaming Assets folder directly on desktop might be better performance-wise, so it might be better to have a different loading method
         * depending on the build, but that's a story for another day (or not).
         * 
         * Also, for the moment, the game only have one music, but we could imagine to have a server that hosts all the musics, and allow the player to download 
         * them, which is REALLY COOL.
         * 
         * Here is the final breakdown :
         */

        // That's the final path of the midi file, in a folder we have access to (since it's persistent, we could check if the file isn't already
        // present, but hey, it's a POC)
        string newPath = Path.Combine(Application.persistentDataPath, music + ".mid");

        // We create the UnityWebRequest, and give it the link of the file we need to download (that is in StreamingAssets). Note that, at this point, the request
        // is only created, not launched.
        UnityWebRequest uwr = UnityWebRequest.Get(Application.streamingAssetsPath + "/MIDI/" + music + ".mid");

        // We create a new DownloadHandlerFile that will help us taking care of the response. Basically, this is the line of code for "hey, when the request
        // is finished, the response will be the content of a file, so save that in the provided path"
        uwr.downloadHandler = new DownloadHandlerFile(newPath);

        // We launch the async request. Yielding the return makes us wait for the response without freezing the game on that frame.
        yield return uwr.SendWebRequest();

        // If we have an error, we do some amazing error management. Yeah, that could be improved because the game crashes in case of error, but hey, that's
        // why it's a POC
        if (uwr.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError(uwr.error);
        }

        // At this point, we have successfully downloaded the midi file in the persistentDataPath and can use it
        MidiFile file = MidiFile.Read(newPath);

        // And now the WetDryMidi magic : we get all the notes in the file and calculate when each note is played (along with other stuff, like beats and
        // pre-notes markers). That gives us a list of events to dispatch and a time to dispatch them, relative to the beginning of the song.
        TempoMap map = file.GetTempoMap();
        foreach (Note note in file.GetNotes())
        {
            float noteStartTimeInSeconds = note.TimeAs<MetricTimeSpan>(map).TotalMicroseconds / 1000000f;
            float oneBeatBeforeStartTime = noteStartTimeInSeconds - map.GetTempoAtTime(note.TimeAs<MetricTimeSpan>(map)).MicrosecondsPerQuarterNote / 1000000f;
            AddMusicEvent(noteStartTimeInSeconds, new MusicEvent(note, MusicEventType.OnNotePlayed));
            AddMusicEvent(oneBeatBeforeStartTime, new MusicEvent(note, MusicEventType.OnOneBeatBeforePlayed));
        }

        // ONLY WORKS WHEN NO TEMPO CHANGES. TODO: tweaks the logic to work with mid-music tempo changes.
        foreach (ValueChange<Tempo> change in map.GetTempoChanges())
        {
            tempoInterval = change.Value.MicrosecondsPerQuarterNote / 1000000f;
            float currentBeatTime = 0;
            while (currentBeatTime < audioSource.clip.length)
            {
                AddMusicEvent(currentBeatTime, new MusicEvent(null, MusicEventType.OnBeat));
                currentBeatTime += tempoInterval;
            }
        }

        loaded = true;
    }

    private void AddMusicEvent(float startTime, MusicEvent musicEvent)
    {
        if (!allMusicEvents.ContainsKey(startTime))
        {
            allMusicEvents.Add(startTime, new List<MusicEvent>());
        }

        allMusicEvents[startTime].Add(musicEvent);
    }

    public void Play()
    {
        StartCoroutine(PlayCoroutine());
    }

    IEnumerator PlayCoroutine()
    {
        while (!loaded)
        {
            yield return new WaitForFixedUpdate();
        }
        OnBeat.Invoke(-4);
        drumStick.Play();
        yield return new WaitForSeconds(tempoInterval);
        OnBeat.Invoke(-3);
        drumStick.Play();
        yield return new WaitForSeconds(tempoInterval);
        OnBeat.Invoke(-2);
        drumStick.Play();
        yield return new WaitForSeconds(tempoInterval);
        OnBeat.Invoke(-1);
        drumStick.Play();
        yield return new WaitForSeconds(tempoInterval);

        float lastTime = 0;
        audioSource.Play();
        timeStart = Time.time;
        foreach (KeyValuePair<float, List<MusicEvent>> kvp in allMusicEvents)
        {
            if (kvp.Key != lastTime)
            {
                yield return new WaitForSeconds(timeStart + kvp.Key - Time.time);
                lastTime = kvp.Key;
            }

            foreach (MusicEvent musicEvent in kvp.Value)
            {
                switch (musicEvent.type)
                {
                    case MusicEventType.OnBeat: currentBeat++; OnBeat.Invoke(currentBeat);break;
                    case MusicEventType.OnOneBeatBeforePlayed: OnOneBeatBeforePlayed.Invoke(musicEvent.note); break;
                    case MusicEventType.OnNotePlayed: onNotePlayed.Invoke(musicEvent.note); break;
                }
            }
        }
        OnMusicEnd.Invoke();
    }

    public bool isThisFrameInBeat()
    {
        return (Time.time - timeStart) % tempoInterval <= 0.15f;
    }
}
