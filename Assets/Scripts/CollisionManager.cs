using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionManager : AutowiredMonoBehaviour
{
    public ContactFilter2D down;
    public ContactFilter2D left;
    public ContactFilter2D up;
    public ContactFilter2D right;

    private bool wasTouchingDown = false;
    private bool wasTouchingLeft = false;
    private bool wasTouchingRight = false;
    private bool wasTouchingUp = false;

    private Rigidbody2D body;

    private void FixedUpdate()
    {
        ManageCollisionChange(ref wasTouchingDown, IsTouchingDown(), "Down");
        ManageCollisionChange(ref wasTouchingLeft, IsTouchingLeft(), "Left");
        ManageCollisionChange(ref wasTouchingRight, IsTouchingRight(), "Right");
        ManageCollisionChange(ref wasTouchingUp, IsTouchingUp(), "Up");
    }

    private void ManageCollisionChange(ref bool oldValue, bool newValue, string direction)
    {
        if (oldValue && !newValue)
        {
            BroadcastMessage("OnLeaving" + direction, SendMessageOptions.DontRequireReceiver);
        }

        if (!oldValue && newValue)
        {
            BroadcastMessage("OnTouching" + direction, SendMessageOptions.DontRequireReceiver);
        }

        oldValue = newValue;
    }

    public bool IsTouchingDown()
    {
        return body.IsTouching(down);
    }

    public bool IsTouchingLeft()
    {
        return body.IsTouching(left);
    }

    public bool IsTouchingRight()
    {
        return body.IsTouching(right);
    }

    public bool IsTouchingUp()
    {
        return body.IsTouching(up);
    }
}
