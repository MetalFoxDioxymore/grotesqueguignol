using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : AutowiredMonoBehaviour
{
    public Animator animator;

    protected string currentState;

    protected void ChangeAnimationState(string newState)
    {
        if (currentState != newState)
        {
            animator.Play(newState);
            currentState = newState;
        }
    }
}
