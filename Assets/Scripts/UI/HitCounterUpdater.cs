using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HitCounterUpdater : MonoBehaviour
{
    public TextMeshPro endMessage;

    private TextMeshPro text;
    private Hit hit;

    void Start()
    {
        text = GetComponent<TextMeshPro>();
        UpdateText(0);
        hit = GameObject.FindGameObjectWithTag("Player").GetComponent<Hit>();
        hit.onHit.AddListener(UpdateText);
    }

    public void UpdateText(int hitNumber)
    {
        text.text = GetFormattedText(hitNumber);
    }


    private string GetFormattedText(int hitNumber)
    {
        return "Hits: " + hitNumber;
    }

    public void DisplayEndMessage()
    {
        endMessage.gameObject.SetActive(true);
        endMessage.text = GetText();
    }

    private string GetText()
    {
        if (hit.hitNumber > 100)
        {
            return "You know you're supposed to avoid them, right ?";
        }

        if (hit.hitNumber >= 70)
        {
            return "That's a start, keep going !";
        }

        if (hit.hitNumber == 69)
        {
            return "Nice";
        }

        if (hit.hitNumber > 50)
        {
            return "Not bad ! Well, not good either, but still solid !";
        }

        if (hit.hitNumber > 30)
        {
            return "Quite nice !";
        }

        if (hit.hitNumber > 20)
        {
            return "You're getting the hang of it !";
        }

        if (hit.hitNumber > 10)
        {
            return "That's honestly really good";
        }

        if (hit.hitNumber > 5)
        {
            return "Impressive ! That's way better than I ever did !";
        }

        if (hit.hitNumber > 1)
        {
            return "Oh my lordies, you're so close !";
        }

        if (hit.hitNumber == 1)
        {
            return "You must be SO MAD right now";
        }

        return "Whaaaaaat, I didn't even know it was possible !";
    }
}
